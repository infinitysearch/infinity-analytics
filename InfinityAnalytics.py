import pymongo
import datetime as dt
from multiprocessing import Process
from urllib.parse import urlparse

from account import mongodb_endpoint

def log_page(path):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityAnalytics']
    col = db['pages']

    date = dt.datetime.now()
    year = date.year
    month = date.month
    day = date.day

    page_today = col.find_one({'path' : path, 'year': year, 'month': month, 'day': day})

    if page_today is None: # If the path has not been viewed today
        col.insert_one({'year': year, 'month': month, 'day': day, 'path': path, 'date' : date, 'views' : 1})
        return True

    # If the page already exists
    views_from_today = page_today['views']

    col.update({'_id': page_today['_id']},{'year': year, 'month': month, 'day': day, 'path': path, 'date' : date, 'views': views_from_today + 1})
    return True


def log_referrer(referrer):
    if referrer is None:
        return False

    parsed = urlparse(referrer)
    referrer = parsed[0] + '://' + parsed[1] + parsed[2]

    client = pymongo.MongoClient(mongodb_endpoint)

    db = client['InfinityAnalytics']
    col = db['referrers']

    date = dt.datetime.now()
    year = date.year
    month = date.month
    day = date.day


    referrer_today = col.find_one({'referrer': referrer, 'year': year, 'month': month, 'day': day})

    if referrer_today is None:  # If there are no referrers from this url today
        col.insert_one({'year': year, 'month': month, 'day': day, 'referrer': referrer, 'date' : date, 'views': 1})
        return True

    # If the referrer already exists
    views_from_today = referrer_today['views']

    col.update({'_id': referrer_today['_id']}, {'year': year, 'month': month, 'day': day, 'date' : date, 'referrer': referrer, 'views': views_from_today + 1})
    return True


def handle_data(request):
    # Multiprocessing to not slow down the Flask request
    p1 = Process(target=log_page, args=(request.path,))
    p2 = Process(target=log_referrer, args=(request.referrer,))
    p1.start()
    p2.start()

