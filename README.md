# Infinity Analytics
A simple analytics platform for tracking pages that are viewed and HTTP referrers.

#### Important: 
- This platform only works for sites that are using the Flask web framework. 
- This is only the code for saving the data to your database. If you have
already integrated this, please go to the [Analytics Dashboard repository](https://gitlab.com/infinitysearch/infinity-analytics-dashboard) and follow the 
instructions from there. 

## Installation 
This analytics platform is optimized to work within the free tiers of MongoDB and Heroku but you
could host your own MongoDB database and Flask server if you wanted to. 

#### Setting up MongoDB 
This analytics platform requires the MongoDB database system. The easiest option
is to create a MongoDB account and get a cloud database at
 [https://www.mongodb.com/cloud/atlas](https://www.mongodb.com/cloud/atlas). You can 
use it for free if you stay under a certain connection and size limit.  Once you do this:
 - Find and click on the connect button on the MongoDB dashboard
 - Click on the connect your application option
 - Change the driver to Python and change the version to 3.6 or later
 - Copy and save the connection string (we will use it later in these instructions)

#### Setting it up on your Flask website:
To run this on your own, you need to: 

1. Install the pymongo package by running:
    ```python
    pip3 install pymongo==3.10.1
    ```
2. Edit the account.py file and change the mongodb_endpoint variable to your own: e.g: 
    ```python
   mongodb_endpoint = 'your_mongodb_connection_string'
    ```
3. Add the InfinityAnalytics.py file to the same directory as your main Flask file.
4. Insert the code below into your Flask logic: 
    ```python
    import InfinityAnalytics as InfinityAnalytics
    @app.before_request
    def before_request():
        InfinityAnalytics.handle_data(request)
    ```

Your website should now be logging page views and HTTP 
referrers. 

#### Adding authorized accounts: 
To view your stats online, you will need to add at least 
one account to be able to login.

To do this, run the AddAccounts.py file and choose option 
1 to add an account. You will choose a username and password
that will be authorized to view the data. You can add 
as many accounts as you need to. You can also delete them 
with option 2. 

## Next Steps
Now all you need to do is deploy your analytics dashboard 
to view a visual representation of your data. The rest of 
the installation is at [https://gitlab.com/infinitysearch/infinity-analytics-dashboard](https://gitlab.com/infinitysearch/infinity-analytics-dashboard).