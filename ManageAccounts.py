import pymongo
import hashlib, binascii, os

from account import mongodb_endpoint


def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password


def add_authorized_account(username, password):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityAnalytics']
    col = db['accounts']

    account = col.find_one({'username': username})

    if account is None:  # If the username is not taken
        password_hash = hash_password(password)
        col.insert_one({'username': username, 'password_hash': password_hash})
        print('Account created successfully')
        return True

    print('Username already taken.')
    return False


def remove_account(username, password):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityAnalytics']
    col = db['accounts']

    account = col.find_one({'username': username})

    if account is None:  # If the username is not in the DB
        print('Username does not exist.')
        return False

    if verify_password(account['password_hash'], password) is False:
        print('Password did not match, account not deleted.')
        return False

    col.delete_one({'username': username, 'password_hash': account['password_hash']})
    print('Account deleted successfully')

    return True


if __name__ == '__main__':
    option = input('To add a user, press 1. \nTo remove a user, press 2.\n$ ')
    if option == '1':
        print('Please fill out the information below to create the account')
        username = input('Username: \n$ ')
        password = input('Password: \n$ ')

        add_authorized_account(username, password)

    elif option == '2':
        print('Please fill out the information below to remove the account')
        username = input('Username: \n$ ')
        password = input('Password: \n$ ')

        remove_account(username, password)
